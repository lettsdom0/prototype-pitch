﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class dummyScript : MonoBehaviour {
    public int dummyHealth = 1;
    public bool Hit = false;
    public bool flameHit = false;
    public Text Health;

    // Use this for initialization
    void Start () {
        
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        //all projectile colliding game objects should be tagged "Enemy" or whatever in inspector but that tag must be reflected in the below if conditional
        if (collision.gameObject.tag == "Normal")
        {
            //If the GameObject's name matches the one you suggest, output this message in the console
            print("i'm hit");
            Debug.Log("Oh no");
            Hit = true;
        }
        if (collision.gameObject.tag == "Fire")
        {
            //If the GameObject's name matches the one you suggest, output this message in the console
            print("i'm on fire!");
            Debug.Log("I'm burnin");
            flameHit = true;
        }

    }

    // Update is called once per frame
    void Update () {
        if (Hit)
        {
            dummyHealth -= 1;
            Hit = false;
        }

        if (flameHit)
        {
            dummyHealth --;
        }


        if (dummyHealth <= 0)
        {
            Destroy(gameObject);
        }
	}
}
