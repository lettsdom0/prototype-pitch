﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class player : MonoBehaviour {

    public Rigidbody2D projectile;
    public Rigidbody2D projectile2;
    public Transform projectileSpawnPoint;
    public float projectileVelocity;
    public float timeBetweenShots;
    private float timeBetweenShotsCounter;
    private bool canShoot;
    public bool norShift;
    // Use this for initialization
    void Start()
    {
        canShoot = true;
        timeBetweenShotsCounter = timeBetweenShots;
        norShift = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            Vector3 position = this.transform.position;
            position.x--;
            this.transform.position = position;
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            Vector3 position = this.transform.position;
            position.x++;
            this.transform.position = position;
        }

        
    }


    private void FixedUpdate()
    {
        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            print("Boop");
            eleToggler();
        }
        
        if (Input.GetKeyDown(KeyCode.Space))
        {
            shooter();
        }
    }

    private void eleToggler()
    {
        print("norShift Toggled!");
        toggler();
    }

    private void toggler()
    {
        if (norShift == true)
        {
            norShift = false;
            print(norShift);
        }

        else if (norShift == false)
        {
            norShift = true;
            print(norShift);
        }
        else
        {
            print("HOW IS THIS POSSIBLE?!");
            print("it's still " + norShift);
        }
    }

    private void shooter()
    {
        print("Patchoo");
        bulletTime();
    }

    private void bulletTime()
    {
        if (norShift == true)
        {
            print("Normal Shot Fired!");
            if (canShoot)
            {
                Rigidbody2D bulletInstance = Instantiate(projectile, projectileSpawnPoint.position, Quaternion.Euler(new Vector3(0, 0, transform.localEulerAngles.z))) as Rigidbody2D;
                bulletInstance.GetComponent<Rigidbody2D>().AddForce(projectileSpawnPoint.right * projectileVelocity);
                canShoot = false;
            }
            if (!canShoot)
            {
                timeBetweenShotsCounter -= Time.deltaTime;
                if (timeBetweenShotsCounter <= 0)
                {
                    canShoot = true;
                    timeBetweenShotsCounter = timeBetweenShots;
                }
            }
            print(norShift);
        }

        else if (norShift == false)
        {
            print("Burning Shot Fired!");
            if (canShoot)
            {
                Rigidbody2D bulletInstance = Instantiate(projectile2, projectileSpawnPoint.position, Quaternion.Euler(new Vector3(0, 0, transform.localEulerAngles.z))) as Rigidbody2D;
                bulletInstance.GetComponent<Rigidbody2D>().AddForce(projectileSpawnPoint.right * projectileVelocity);
                canShoot = false;
            }
            if (!canShoot)
            {
                timeBetweenShotsCounter -= Time.deltaTime;
                if (timeBetweenShotsCounter <= 0)
                {
                    canShoot = true;
                    timeBetweenShotsCounter = timeBetweenShots;
                }
            }
            print(norShift);
        }
        else
        {
            print("No ammo... wait wha-");
            print("it's still " + norShift);
        }
    }
}
