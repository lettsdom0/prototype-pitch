﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swordSlash : MonoBehaviour {
    public float x;
    public float y;
    private Rigidbody2D rb2d;
    

    // Use this for initialization
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
    }
    
    void OnCollisionEnter2D(Collision2D collision)
    {
        print("fired");
        //all projectile colliding game objects should be tagged "Enemy" or whatever in inspector but that tag must be reflected in the below if conditional
        if (collision.gameObject.tag == "Dummy")
        {
            //If the GameObject's name matches the one you suggest, output this message in the console
            print("gotcha!");
            Debug.Log("direct hit!");
            Destroy(gameObject);
        }

    }

    // Update is called once per frame
    void Update()
    {
        this.GetComponent<Rigidbody2D>().transform.Translate(new Vector3(x, y, 0));
    }
}
